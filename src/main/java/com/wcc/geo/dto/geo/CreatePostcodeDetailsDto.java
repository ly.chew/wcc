package com.wcc.geo.dto.geo;

import com.wcc.geo.config.annotation.UKPostcode;
import lombok.Data;

@Data
public class CreatePostcodeDetailsDto {

    @UKPostcode
    String postcode;

    double latitude;

    double longitude;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreatePostcodeDetailsDto that = (CreatePostcodeDetailsDto) o;
        return this.postcode.equalsIgnoreCase(that.getPostcode());
    }

    @Override
    public int hashCode() {
        return postcode.hashCode();
    }
}
