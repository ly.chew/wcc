package com.wcc.geo.dto.geo;

import lombok.Data;

@Data
public class DistanceResultDto {

    String postcode1;

    double latitude1;

    double longitude1;

    String postcode2;

    double latitude2;

    double longitude2;

    double distance;

    String unit = "km";

}
