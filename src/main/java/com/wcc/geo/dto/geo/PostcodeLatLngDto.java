package com.wcc.geo.dto.geo;

import lombok.Data;

@Data
public class PostcodeLatLngDto {

    String postcode;

    double latitude;

    double longitude;
}
