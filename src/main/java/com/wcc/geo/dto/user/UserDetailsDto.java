package com.wcc.geo.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDetailsDto {

    String id;

    String staffNo;

    String identity;

    String name;

    String email;

    Integer category;

    String ptCode;

    String ptCodeNew;

    String designation;

    String contact;

}
