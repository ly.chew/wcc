package com.wcc.geo.dto.user;


import com.wcc.geo.config.annotation.ValidPassword;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class CreateUserDto {

    @NotBlank(message = "Email should not be blank")
    @Email(message = "Invalid email address given")
    String email;

    @NotBlank(message = "Name should not be blank")
    String name;

    @NotBlank(message = "Password should not be blank")
    @ValidPassword
    String password;
    
}
