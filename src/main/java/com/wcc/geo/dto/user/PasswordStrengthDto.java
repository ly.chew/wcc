package com.wcc.geo.dto.user;

import lombok.Data;

@Data
public class PasswordStrengthDto {

    private int minLength = 8;

    private boolean specialCharacter = false;

    private boolean uppercase = true;

    private boolean lowercase = true;

    private boolean number = true;

    private String allowedSpecialCharacter;

    public PasswordStrengthDto(int minLength, boolean specialCharacter, boolean uppercase, boolean lowercase, boolean number, String allowedSpecialCharacter) {
        this.minLength = minLength;
        this.specialCharacter = specialCharacter;
        this.uppercase = uppercase;
        this.lowercase = lowercase;
        this.number = number;
        this.allowedSpecialCharacter = allowedSpecialCharacter;
    }
}
