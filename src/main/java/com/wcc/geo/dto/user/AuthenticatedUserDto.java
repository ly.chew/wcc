package com.wcc.geo.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthenticatedUserDto {

    UserDetailsDto user;

    TokenDto tokens;

    public AuthenticatedUserDto() {
    }

    public AuthenticatedUserDto(UserDetailsDto user, TokenDto tokens) {
        this.user = user;
        this.tokens = tokens;
    }
}
