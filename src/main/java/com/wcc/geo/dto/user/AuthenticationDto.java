package com.wcc.geo.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthenticationDto {

    @NotBlank(message = "Email should not be blank")
    @Email(message = "Invalid email given")
    private String email;

    @NotBlank(message = "Password should not be blank")
    private String password;

}
