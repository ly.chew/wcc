package com.wcc.geo.dto.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RefreshTokenDto {

    @NotBlank(message = "Refresh token should not be blank")
    String refreshToken;
}
