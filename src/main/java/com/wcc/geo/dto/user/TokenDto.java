package com.wcc.geo.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TokenDto {

    String idToken;

    Integer accessCode;

    String refreshToken;

    public TokenDto() {
    }

    public TokenDto(String idToken) {
        this.idToken = idToken;
    }

    public TokenDto(String idToken, String refreshToken) {
        this.idToken = idToken;
        this.refreshToken = refreshToken;
    }

    public TokenDto(String idToken, String refreshToken, Integer accessCode) {
        this.idToken = idToken;
        this.accessCode = accessCode;
        this.refreshToken = refreshToken;
    }
}
