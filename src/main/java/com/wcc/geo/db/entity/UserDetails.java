package com.wcc.geo.db.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.UUID;

@Entity
@Table(name = "user_detail")
@Data
@NoArgsConstructor
public class UserDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    UUID id;


    @Column(unique = true)
    String email;

    String password;

    String name;

    @Column(columnDefinition = "boolean default false")
    private boolean disabled;

    @Column(name = "created_at")
    private long createdAt;

    @Column(name = "last_modified")
    private long modifiedAt;

    public UserDetails(UUID id) {
        this.id = id;
    }


    @PrePersist
    public void prePersist() {
        this.createdAt = LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond();
        this.modifiedAt = this.createdAt;
    }

    @PreUpdate
    public void preUpdate() {
        this.modifiedAt = LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond();
    }

}
