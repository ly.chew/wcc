package com.wcc.geo.db.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "postcodelatlng")
@Data
public class PostcodeLatLng {

    @Id
    String postcode;

    double latitude;

    double longitude;

}
