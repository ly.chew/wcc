package com.wcc.geo.db.response;

import lombok.Data;

@Data
public class ConstraintValidatorResponse {
    long timestamp;

    int status;

    String error;

    String message;

    String path;
}
