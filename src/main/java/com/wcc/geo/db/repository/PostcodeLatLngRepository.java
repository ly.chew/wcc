package com.wcc.geo.db.repository;

import com.wcc.geo.db.entity.PostcodeLatLng;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;


@Transactional
public interface PostcodeLatLngRepository extends CrudRepository<PostcodeLatLng, String> {

//    Optional<PostcodeLatLng> findByPostcode(String postcode);

    @Query(value = "SELECT p.postcode from PostcodeLatLng p WHERE p.postcode IN (:postcodes)")
    List<String> findPostcodeByPostcodeIn(@Param("postcodes") Set<String> postcodes);

    void deleteByPostcodeIn(Set<String> postcodes);

    boolean existsByPostcode(String postcode);

}
