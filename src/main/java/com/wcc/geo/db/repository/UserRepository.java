package com.wcc.geo.db.repository;

import com.wcc.geo.db.entity.UserDetails;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends CrudRepository<UserDetails, UUID> {

    Optional<UserDetails> findByEmail(String email);

    boolean existsByEmail(String email);

    List<UserDetails> findAll(Specification<UserDetails> specs, Pageable pageable);

    @Query(value = "select password from user_detail where id = :userId", nativeQuery = true)
    String findPasswordById(@Param("userId") UUID userId);
}