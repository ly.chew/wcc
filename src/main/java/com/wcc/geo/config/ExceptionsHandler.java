package com.wcc.geo.config;

import com.wcc.geo.db.response.ConstraintValidatorResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionsHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity handle(ConstraintViolationException constraintViolationException, WebRequest request) {
        ConstraintValidatorResponse body = new ConstraintValidatorResponse();
        Set<ConstraintViolation<?>> violations = constraintViolationException.getConstraintViolations();
        String errorMessage = "";
        if (!violations.isEmpty()) {
//            StringBuilder builder = new StringBuilder();
//            violations.forEach(violation -> builder.append(violation.getMessage()));
            errorMessage = String.join(",", violations.stream().map(v -> v.getMessage()).collect(Collectors.toSet()));
        } else {
            errorMessage = "ConstraintViolationException occured.";
        }
        body.setTimestamp(new Date().getTime());
        body.setMessage(errorMessage);
        body.setPath(((ServletWebRequest) request).getRequest().getRequestURI().toString());
        body.setStatus(HttpStatus.BAD_REQUEST.value());
        body.setError(HttpStatus.BAD_REQUEST.getReasonPhrase());

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {

        ConstraintValidatorResponse body = new ConstraintValidatorResponse();
        Optional<FieldError> fieldErrorOpt = ex.getBindingResult().getFieldErrors().stream().findFirst();
        if (fieldErrorOpt.isPresent()) {
            body.setMessage(fieldErrorOpt.get().getDefaultMessage());
        }
        body.setError(status.getReasonPhrase());
        body.setTimestamp(new Date().getTime());
        body.setPath(((ServletWebRequest) request).getRequest().getRequestURI());
        body.setStatus(status.value());

        return new ResponseEntity<>(body, headers, status);
    }
}
