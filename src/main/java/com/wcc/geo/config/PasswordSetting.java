package com.wcc.geo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "security.password")
@Data
public class PasswordSetting {

    //    private static final String PASSWORD_ALLOWED_SPECIAL_CHARACTER = "#?!@$%^&*_+-=:;<>,.";
    private static final String PASSWORD_REGEX_START = "^";
    //    private static final String PASSWORD_REGEX_SPECIAL_CHARACTER = "(?=.*[*.!@$%^&(){}[]:;<>,.?/~_+-=|\\])";
    private static final String PASSWORD_REGEX_NUMBER = "(?=.*[0-9])";
    private static final String PASSWORD_REGEX_UPPERCASE = "(?=.*[A-Z])";
    private static final String PASSWORD_REGEX_LOWERCASE = "(?=.*[a-z])";
    private static final String PASSWORD_REGEX_END = "$";
    private String PASSWORD_REGEX_SPECIAL_CHARACTER;

    private String hashSecret;

    private int hashIteration = 200000,
            hashWidth = 200000;

    private int length = 8;

    private int validationCodeLength = 6;

    private long validationCodeExpiry = 600;

    private boolean number = true,
            special = false,
            upper = true,
            lower = true;

    private String specialAllowed = "#?!@$%^&*_+=:;<>,";


    public PasswordSetting() {
        PASSWORD_REGEX_SPECIAL_CHARACTER = String.format("(?=.*[%s])", this.specialAllowed);  //(?=.*[" + USER_AUTH_PASSWORD_SPECIAL_ALLOWED + "])";
    }

    public String getPasswordRegex() {
        String regex = PASSWORD_REGEX_START;

        if (this.number) {
            regex += PASSWORD_REGEX_NUMBER;
        }


        if (this.lower) {
            regex += PASSWORD_REGEX_LOWERCASE;
        }

        if (this.upper) {
            regex += PASSWORD_REGEX_UPPERCASE;
        }

        if (this.special) {
            regex += PASSWORD_REGEX_SPECIAL_CHARACTER;
        }

        regex += ".{" + this.length + ",}" + PASSWORD_REGEX_END;

        return regex;
    }

    public boolean isCodeLengthValid(int code){
        int length = String.valueOf(code).length();
        return length == this.getValidationCodeLength();
    }
}
