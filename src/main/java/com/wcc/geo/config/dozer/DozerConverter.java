package com.wcc.geo.config.dozer;

import com.github.dozermapper.core.Mapper;
import org.springframework.core.convert.converter.GenericConverter.ConvertiblePair;

import java.util.Collections;
import java.util.Set;

public class DozerConverter implements BeanConverter {

	private Mapper mapper;
	
	public Set<ConvertiblePair> getConvertibleTypes() {
		return Collections.singleton(new ConvertiblePair(Object.class, Object.class));
	}

	public <T> T convert(Object source, Class<T> targetClass) {
		return mapper.map(source, targetClass);
	}

	public Mapper getMapper() {
		return mapper;
	}

	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

}
