package com.wcc.geo.config.annotation;

import com.wcc.geo.validator.PasswordConstraintValidator;
import com.wcc.geo.validator.UKPostcodeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Documented
@Constraint(validatedBy = UKPostcodeValidator.class)
@Target({TYPE, FIELD, ANNOTATION_TYPE,PARAMETER,TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface UKPostcode {

    String message() default "Invalid UK postcode '${validatedValue}'. Please refer https://www.mrs.org.uk/pdf/postcodeformat.pdf for a valid postcode";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
