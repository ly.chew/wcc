package com.wcc.geo.validator;

import com.wcc.geo.config.annotation.UKPostcode;
import com.wcc.geo.util.GeoUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UKPostcodeValidator implements ConstraintValidator<UKPostcode, String> {


    @Override
    public void initialize(UKPostcode constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) return false;

        return GeoUtil.validatePostcode(value);
    }
}
