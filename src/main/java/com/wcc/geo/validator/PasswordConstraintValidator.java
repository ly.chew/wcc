package com.wcc.geo.validator;

import com.wcc.geo.config.PasswordSetting;
import com.wcc.geo.config.annotation.ValidPassword;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

    @Autowired
    PasswordSetting passwordSetting;

    @Override
    public void initialize(ValidPassword constraintAnnotation) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        String passwordRegex = passwordSetting.getPasswordRegex();
        Pattern pattern = Pattern.compile(passwordRegex);
        return pattern.matcher(s).matches();
    }
}
