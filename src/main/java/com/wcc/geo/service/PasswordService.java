package com.wcc.geo.service;

import com.wcc.geo.config.PasswordSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordService {
    @Autowired
    PasswordSetting passwordSetting;

    private Pbkdf2PasswordEncoder encoder = null;


    private Pbkdf2PasswordEncoder getEncoder() {
        if (this.encoder == null) {
            this.encoder = new Pbkdf2PasswordEncoder(
                    passwordSetting.getHashSecret(), passwordSetting.getHashIteration(), passwordSetting.getHashWidth()
            );
            this.encoder.setEncodeHashAsBase64(true);
        }

        return this.encoder;
    }

    public boolean isCodeLengthValid(int code){
        return this.passwordSetting.isCodeLengthValid(code);
    }

    public int getValidationCodeLength(){
        return this.passwordSetting.getValidationCodeLength();
    }

    public long getValidationCodeExpiry(){
        return this.passwordSetting.getValidationCodeExpiry();
    }

    public String getHashPassword(String rawPassword) {
        return this.getEncoder().encode(rawPassword);
    }

    public boolean matchHashPassword(String rawPassword, String encodedPassword) {
        return this.getEncoder().matches(rawPassword, encodedPassword);
    }

    public int getPasswordLength() {
        return passwordSetting.getLength();
    }

    public boolean isNumberRequired() {
        return passwordSetting.isNumber();
    }

    public boolean isUpperCaseRequired() {
        return passwordSetting.isUpper();
    }

    public boolean isLowerCaseRequired() {
        return passwordSetting.isLower();
    }

    public boolean isSpecialCharacterRequired(){
        return passwordSetting.isSpecial();
    }

    public String getAllowedSpecialCharacter(){
        return passwordSetting.getSpecialAllowed();
    }
}
