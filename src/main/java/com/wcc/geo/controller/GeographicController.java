package com.wcc.geo.controller;

import com.wcc.geo.config.annotation.UKPostcode;
import com.wcc.geo.dto.geo.CreatePostcodeDetailsDto;
import com.wcc.geo.dto.geo.DistanceResultDto;
import com.wcc.geo.dto.geo.PostcodeLatLngDto;
import com.wcc.geo.dto.geo.UpdatePostcodeDetailsDto;
import com.wcc.geo.facade.GeographicRequestFacade;
import com.wcc.geo.util.GeoUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Set;

@RestController
@RequestMapping("/api/geo")
@Validated
public class GeographicController {

    @Autowired
    GeographicRequestFacade geographicRequestFacade;

    @GetMapping("/postcode/{postcode}")
    @ApiOperation("Get latitude and longitude by postcode")
    public ResponseEntity<PostcodeLatLngDto> getPostcodeDetails(
            @UKPostcode
            @PathVariable("postcode") String postcode
    ) {
        PostcodeLatLngDto postcodeLatLngDto = geographicRequestFacade.getPostcodeDetails(postcode);

        return ResponseEntity.ok(postcodeLatLngDto);
    }

    @PutMapping("/postcodes")
    @ApiOperation("Update multiple latitude and longitude by postcodes.")
    public ResponseEntity<Void> updatePostcodeDetails(
            @RequestBody
            @Size(min = 1) Set<@Valid UpdatePostcodeDetailsDto> postcodeDetailsDtos
    ) {
        geographicRequestFacade.updatePostcodeDetails(postcodeDetailsDtos);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/postcodes")
    @ApiOperation("Add multiple new postcodes with latitude and longitude.")
    public ResponseEntity<Void> addNewPostcodeDetails(
            @RequestBody
            @Size(min = 1) Set<@Valid CreatePostcodeDetailsDto> postcodeDetailsDtos
    ) {
        geographicRequestFacade.createNewPostcodeDetails(postcodeDetailsDtos);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @DeleteMapping("/postcodes")
    @ApiOperation("Delete multiple postcodes.")
    public ResponseEntity<Void> deleteMultiplePostcodes(
            @RequestBody Set<@UKPostcode String> postcodes
    ) {
        geographicRequestFacade.deleteMultiplePostcodes(postcodes);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/distance")
    @ApiOperation("Get distance between 2 postcodes.")
    public ResponseEntity<DistanceResultDto> getDistanceBetweenPostcodes(
            @UKPostcode
            @RequestParam("postcode1") String postcode1,
            @UKPostcode
            @RequestParam("postcode2") String postcode2
    ) {
        DistanceResultDto result = geographicRequestFacade.getDistanceBetweenPostcodes(postcode1, postcode2);

        return ResponseEntity.ok(result);
    }

    @GetMapping("/validate/postcode/{postcode}")
    @ApiOperation("Validate postcode")
    public ResponseEntity<Boolean> validatePostcode(
            @PathVariable("postcode") String postcode
    ) {
        return ResponseEntity.ok(GeoUtil.validatePostcode(postcode));
    }
}
