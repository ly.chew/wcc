package com.wcc.geo.controller;

import com.wcc.geo.dto.user.*;
import com.wcc.geo.facade.UserRequestFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
@Validated
public class AuthController {

    @Autowired
    UserRequestFacade userRequestFacade;

    @PostMapping("/login")
    public ResponseEntity<AuthenticatedUserDto> login(@Valid @RequestBody AuthenticationDto authenticationDto) throws Exception {
        AuthenticatedUserDto authenticatedUserDto = userRequestFacade.authenticateUser(authenticationDto);
        return new ResponseEntity<>(authenticatedUserDto, HttpStatus.OK);
    }

    @PostMapping("/refresh")
    public ResponseEntity<AuthenticatedUserDto> refreshToken(@Valid @RequestBody RefreshTokenDto refreshTokenDto) throws Exception {
        AuthenticatedUserDto authenticatedUserDto = userRequestFacade.refreshToken(refreshTokenDto);
        return new ResponseEntity<>(authenticatedUserDto,HttpStatus.OK);
    }


}
