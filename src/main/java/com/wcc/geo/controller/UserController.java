package com.wcc.geo.controller;

import com.wcc.geo.dto.user.CreateUserDto;
import com.wcc.geo.dto.user.PasswordStrengthDto;
import com.wcc.geo.dto.user.UserDetailsDto;
import com.wcc.geo.facade.UserRequestFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserRequestFacade userRequestFacade;

    @PostMapping("/add")
    public ResponseEntity<UserDetailsDto> createUser(
            @Valid @RequestBody CreateUserDto createUserModel
    ) {
        UserDetailsDto created = userRequestFacade.createUser(createUserModel);

        return ResponseEntity.ok(created);
    }

    @GetMapping("/password/strength")
    public ResponseEntity<PasswordStrengthDto> getPasswordStrength() {
        PasswordStrengthDto passwordStrength = userRequestFacade.getPasswordStrength();
        return ResponseEntity.ok(passwordStrength);
    }
}
