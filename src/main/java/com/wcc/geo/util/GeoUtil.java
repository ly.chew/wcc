package com.wcc.geo.util;

import lombok.extern.slf4j.Slf4j;

import java.util.regex.Pattern;

@Slf4j
public class GeoUtil {

    /*
        This regex was built based on url:
        https://www.mrs.org.uk/pdf/postcodeformat.pdf
    */
    public static final String REGEX_UK_POSTCODE = "^[A-PR-UWYZa-pr-uwyz]([A-HK-Ya-hk-y]?([0-9]{1,2}|[0-9][A-HJKS-UWa-hjks-uw])) [0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}$";

    public static final double EARTH_RADIUS = 6371;

    public static boolean validatePostcode(String postcode) {
        Pattern pattern = Pattern.compile(REGEX_UK_POSTCODE);
        return pattern.matcher(postcode).matches();
    }

    public static double getDistance(double lat1, double long1, double lat2, double long2) {
        double long1Radian = Math.toRadians(long1);
        double long2Radian = Math.toRadians(long2);
        double lat1Radian = Math.toRadians(lat1);
        double lat2Radian = Math.toRadians(lat2);

        log.info("lat1:{},long1:{},lat2:{},long2:{}", lat1Radian, long1Radian, lat2Radian, long2Radian);

        double a = haversine(lat1Radian, lat2Radian) + Math.cos(lat1Radian) * Math.cos(lat2Radian) * haversine(long1Radian, long2Radian);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (EARTH_RADIUS * c);
    }

    private static double haversine(double deg1, double deg2) {
        return Math.pow(Math.sin((deg1 - deg2) / 2.0), 2);
    }

}
