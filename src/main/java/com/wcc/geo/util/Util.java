package com.wcc.geo.util;

import lombok.extern.slf4j.Slf4j;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class Util {

    public static LocalDateTime getCurrentTime() {
        return LocalDateTime.now();
    }

    public static <T, U> List<U> convertList(Set<T> from, Function<T, U> func) {
        return from.stream().map(func).collect(Collectors.toList());
    }


    public static Long convertTimeToMiliSeconds(Date time) {
        if (time != null) {

            try {
                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date reference = dateFormat.parse(time.toString());
//            long seconds = time.getTime() - reference.getTime();
                return reference.getTime();
            } catch (Exception e) {
                log.error("Failed to convert time to mili seconds, {}", e.getMessage());
            }
        }

        return null;
    }

    public static Long convertTimeToMiliSeconds(String time) {
        if (time != null) {

            try {
                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date reference = dateFormat.parse(time);
//            long seconds = time.getTime() - reference.getTime();
                return reference.getTime();
            } catch (Exception e) {
                log.error("Failed to convert time to mili seconds, {}", e.getMessage());
            }
        }

        return null;
    }

    public static Long convertDateTimeToMiliSeconds(LocalDateTime dateTime) {
        if (dateTime == null) {
            return null;
        }

        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    public static LocalDateTime convertMiliSecondsToDateTime(Long miliseconds) {
        if (miliseconds != null) {
            return LocalDateTime.ofInstant(Instant.ofEpochMilli(miliseconds), ZoneId.systemDefault());
        }
        return null;
    }

    public static long getMiliseconds(LocalDateTime time) {
        if (time != null) {
            return time.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        }

        return new Date().getTime();
    }

    public static long getSeconds(LocalDateTime time) {
        return Math.round(getMiliseconds(time) / 1000);
    }

}
