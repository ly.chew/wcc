package com.wcc.geo.facade;

import com.wcc.geo.config.dozer.BeanConverter;
import com.wcc.geo.db.entity.PostcodeLatLng;
import com.wcc.geo.db.repository.PostcodeLatLngRepository;
import com.wcc.geo.dto.geo.CreatePostcodeDetailsDto;
import com.wcc.geo.dto.geo.DistanceResultDto;
import com.wcc.geo.dto.geo.PostcodeLatLngDto;
import com.wcc.geo.dto.geo.UpdatePostcodeDetailsDto;
import com.wcc.geo.exception.BadRequestException;
import com.wcc.geo.exception.SourceNotFoundException;
import com.wcc.geo.util.GeoUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GeographicRequestFacade {
    @Autowired
    BeanConverter beanConverter;

    @Autowired
    PostcodeLatLngRepository postcodeLatLngRepository;

    public PostcodeLatLngDto getPostcodeDetails(String postcode) {
        PostcodeLatLng postcodeLatLng = postcodeLatLngRepository.findById(postcode)
                .orElseThrow(() -> new SourceNotFoundException(String.format("Postcode '%s' not found.", postcode)));

        return beanConverter.convert(postcodeLatLng, PostcodeLatLngDto.class);
    }

    public void updatePostcodeDetails(Set<UpdatePostcodeDetailsDto> updatePostcodeDetailsDtos) {

        List<String> postcodes = updatePostcodeDetailsDtos.stream().map(UpdatePostcodeDetailsDto::getPostcode).collect(Collectors.toList());

        Map<String, PostcodeLatLng> postcodeMap = ((ArrayList<PostcodeLatLng>) postcodeLatLngRepository.findAllById(postcodes))
                .stream()
                .collect(Collectors.toMap(PostcodeLatLng::getPostcode, p -> p));

        List<PostcodeLatLng> toUpdate = new ArrayList<>();
        for (UpdatePostcodeDetailsDto updatePostcode : updatePostcodeDetailsDtos) {
            String postcode = updatePostcode.getPostcode();

            PostcodeLatLng postcodeLatLng = postcodeMap.get(postcode);

            if (postcodeLatLng == null) {
                throw new SourceNotFoundException(String.format("Postcode '%s' not found", postcode));
            }

            log.info("Update Postcode: {}, Lat:{} >> {}, Lng:{} >> {}",
                    postcode,
                    postcodeLatLng.getLatitude(),
                    updatePostcode.getLatitude(),
                    postcodeLatLng.getLongitude(),
                    updatePostcode.getLongitude()
            );

            postcodeLatLng.setLatitude(updatePostcode.getLatitude());
            postcodeLatLng.setLongitude(updatePostcode.getLongitude());

            toUpdate.add(postcodeLatLng);
        }

        postcodeLatLngRepository.saveAll(toUpdate);
        log.info("Updated {} postcode(s)", toUpdate.size());
    }

    public void createNewPostcodeDetails(Set<CreatePostcodeDetailsDto> createPostcodeDetailsDtos) {
        Set<String> postcodes = createPostcodeDetailsDtos.stream().map(CreatePostcodeDetailsDto::getPostcode).collect(Collectors.toSet());

        List<String> existingPostcode = postcodeLatLngRepository.findPostcodeByPostcodeIn(postcodes);

        if (existingPostcode.size() > 0) {
            throw new BadRequestException(String.format("Postcode(s) %s already exists", String.join(",", existingPostcode)));
        }

        log.info("Saving new postcodes: {}", postcodes);

        List<PostcodeLatLng> postcodeLatLngs = createPostcodeDetailsDtos.stream()
                .map(n -> beanConverter.convert(n, PostcodeLatLng.class))
                .collect(Collectors.toList());

        postcodeLatLngRepository.saveAll(postcodeLatLngs);
    }

    public void deleteMultiplePostcodes(Set<String> postcodes) {
        log.info("Deleting postcodes: {}", postcodes);
        postcodeLatLngRepository.deleteByPostcodeIn(postcodes);
    }

    public DistanceResultDto getDistanceBetweenPostcodes(String postcode1, String postcode2) {

        if (postcode1.equalsIgnoreCase(postcode2)) {
            throw new BadRequestException("Both postcodes should not be the same.");
        }

        PostcodeLatLng postcodeLatLng1 = postcodeLatLngRepository.findById(postcode1)
                .orElseThrow(() -> new SourceNotFoundException(String.format("Postcode '%s' not found.", postcode1)));


        PostcodeLatLng postcodeLatLng2 = postcodeLatLngRepository.findById(postcode2)
                .orElseThrow(() -> new SourceNotFoundException(String.format("Postcode '%s' not found.", postcode2)));


        double distance = GeoUtil.getDistance(
                postcodeLatLng1.getLatitude(), postcodeLatLng1.getLongitude(),
                postcodeLatLng2.getLatitude(), postcodeLatLng2.getLongitude());

        log.info("Postcodes: {} -> {},Distance in KM: {}", postcode1, postcode2, distance);


        DistanceResultDto dto = new DistanceResultDto();

        dto.setDistance(distance);
        dto.setPostcode1(postcode1);
        dto.setPostcode2(postcode2);

        return dto;
    }

}
