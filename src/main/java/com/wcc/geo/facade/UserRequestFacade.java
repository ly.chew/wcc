package com.wcc.geo.facade;

import com.nimbusds.jwt.JWTClaimsSet;
import com.wcc.geo.config.dozer.BeanConverter;
import com.wcc.geo.db.entity.UserDetails;
import com.wcc.geo.db.repository.UserRepository;
import com.wcc.geo.dto.user.*;
import com.wcc.geo.exception.BadRequestException;
import com.wcc.geo.exception.UnauthorizedException;
import com.wcc.geo.service.JWTService;
import com.wcc.geo.service.PasswordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserRequestFacade {

    @Autowired
    PasswordService passwordService;

    @Autowired
    JWTService jwtService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BeanConverter beanConverter;

    public AuthenticatedUserDto authenticateUser(AuthenticationDto authenticationDto) throws Exception {
        Optional<UserDetails> userOpt = userRepository.findByEmail(authenticationDto.getEmail());

        if (!userOpt.isPresent()) {
            log.info("Unauthorized user {}", authenticationDto.getEmail());
            throw new UnauthorizedException("Invalid email or password");
        }

        UserDetails userDetails = userOpt.get();

        if (userDetails.isDisabled()) {
            throw new BadRequestException("Account was disabled");
        }

        boolean isMatched = passwordService.matchHashPassword(authenticationDto.getPassword(), userDetails.getPassword());

        if (!isMatched) {
            log.info("Unauthorized user {} with password {}", authenticationDto.getEmail(), authenticationDto.getPassword());
            throw new UnauthorizedException("Invalid email or password");
        }

        log.info("User {} authenticated, generating Token", authenticationDto.getEmail());

        UserDetailsDto userDetailsDto = beanConverter.convert(userDetails, UserDetailsDto.class);

        String idToken = jwtService.generateIdToken(userDetailsDto);
        String refreshToken = jwtService.generateRefreshToken(userDetailsDto);

        TokenDto token = new TokenDto(idToken, refreshToken);

        return new AuthenticatedUserDto(userDetailsDto, token);

    }

    public AuthenticatedUserDto refreshToken(RefreshTokenDto refreshTokenDto) throws Exception {
        JWTClaimsSet claimsSet = jwtService.verifyRefreshToken(refreshTokenDto.getRefreshToken());

        log.info("Refresh token expiry: {}", claimsSet.getExpirationTime());

        UserDetailsDto userDetailsDto = jwtService.getUserDetailsFromJWTClaimSet(claimsSet);

        String idToken = jwtService.generateIdToken(userDetailsDto);

        TokenDto token = new TokenDto(idToken);

        return new AuthenticatedUserDto(userDetailsDto, token);
    }

    public UserDetailsDto createUser(CreateUserDto createUser) {
        log.info("Creating new user {}", createUser.getEmail());

        if (existsByEmail(createUser.getEmail())) {
            log.info("Email already exists");
            throw new BadRequestException("Email already exists");
        }

        UserDetails userDetails = new UserDetails();
        userDetails.setName(createUser.getName());
        userDetails.setEmail(createUser.getEmail());
        userDetails.setPassword(passwordService.getHashPassword(createUser.getPassword()));

        UserDetails newUserDetails = userRepository.save(userDetails);

        log.info("Created new user successful");

        return beanConverter.convert(newUserDetails, UserDetailsDto.class);
    }

    public PasswordStrengthDto getPasswordStrength() {
        return new PasswordStrengthDto(
                passwordService.getPasswordLength(),
                passwordService.isSpecialCharacterRequired(),
                passwordService.isUpperCaseRequired(),
                passwordService.isLowerCaseRequired(),
                passwordService.isNumberRequired(),
                passwordService.getAllowedSpecialCharacter()
        );
    }

    private boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }
}
